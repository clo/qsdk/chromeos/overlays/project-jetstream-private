# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

DESCRIPTION="Jetstream-specific packages for the root of a dev image"
HOMEPAGE="http://dev.chromium.org/"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND="
	net-misc/dhcp
	net-misc/radvd"

S=${WORKDIR}

src_install() {
	insinto /etc/init
	doins "${FILESDIR}"/etc/init/*
}
