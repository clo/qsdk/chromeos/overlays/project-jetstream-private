# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit udev

DESCRIPTION="Jetstream-specific packages for the root of a test image"
HOMEPAGE="http://dev.chromium.org/"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE=""

RDEPEND="
	app-admin/sysstat
	chromeos-base/nlcli
	chromeos-base/spi-server
	net-dns/bind-tools
	"

S="${WORKDIR}"

src_install() {
	# Install factory specific inhibit-* upstart jobs
	insinto /etc/init
	doins "${FILESDIR}"/factory/etc/init/*

	# Install upstart jobs used in test build.
	insinto /etc/init
	doins "${FILESDIR}"/test/etc/init/*
	dosym  /usr/local/lib/sa/sadc /usr/local/bin/sadc

        # Install udev and dhclient tools to manage USB test interfaces.
	udev_dorules "${FILESDIR}"/test/lib/udev/rules.d/*
	exeinto /lib/udev
	doexe "${FILESDIR}"/test/lib/udev/*
	insinto /etc
	doins "${FILESDIR}"/test/etc/dhclient_usb_test_interface.conf
}
