#!/bin/bash

# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

action="$1"
interface="$2"

metric=30

# Do not use /var/run; rules may run before it is mounted.
pidfile="/tmp/dhclient-usb-test-interface-${interface}.pid"
leasefile="/tmp/dhclient-usb-test-interface.leases"
configfile="/etc/dhclient_usb_test_interface.conf"

case "${action}" in
  add)
    # At boot time, the udev rule can fire multiple times.  Make sure that only
    # one instance of dhclient is started.
    if pgrep -f "dhclient.* ${interface}" 2>/dev/null >/dev/null; then
      exit 0
    fi

    # Loosen reverse path filtering to enable asymmetric routing.  This allows
    # WAN and test interfaces to be on the same subnet.  In a lab testbed, this
    # is not deal, but for developer testing, it's helpful.  Doing this here
    # means we can leave dhclient-script unmodified.
    echo 2 > /proc/sys/net/ipv4/conf/${interface}/rp_filter
    iptables -A OUTPUT -o ${interface} -p udp -m udp --dport 53 -j DROP

    /sbin/dhclient -nw -e IF_METRIC=${metric} -e PEER_DNS=no \
        -lf "${leasefile}" -pf "${pidfile}" -cf "${configfile}" "${interface}"
    ;;

  remove)
    if [[ -e "${pidfile}" ]]; then
      kill $(cat "${pidfile}")
      rm "${pidfile}"
    fi
    ;;
esac
