#!/bin/bash

# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

. /usr/share/ap/constants.sh

log_name=$(basename $0)

abort_if_interface_exists() {
  local name="$1"
  if [ -d /sys/class/net/${name} ]; then
    logger -t "${log_name}" "Interface ${name} already exists, aborting!"
    exit 1
  fi
}

configure_allspark_g() {
  logger -t "${log_name}" "detected ${INTERFACE} as Allspark-G"

  # just in case a previous rule already started dhclient on it
  ifconfig ${INTERFACE} 0.0.0.0 down

  # WAN0 can be used w/o additional tagging since the FW sets the
  # default tag to 1 if no tag is present in a packet.
  # To use both ports, the host needs to setup vlan tagging.
  ip link add link "${INTERFACE}" name "${WAN0}" type vlan id 1
  ip link add link "${INTERFACE}" name "${LAN0}" type vlan id 2

  # Rename the underlying interface to prevent it being picked up by udev rules
  # designed to handle unrenamed USB ethernet adapters.
  ip link set dev "${INTERFACE}" name "allsparkg" up

  brctl addif "${LAN_BRIDGE}" "${LAN0}"
}

# udev rule will be triggered _again_ by previous rename
case "${INTERFACE}" in
  "${WAN0}" | "${LAN0}" | allsparkg)
    logger -t "${log_name}" "${INTERFACE} already configured"
    exit 0
    ;;
esac

case "${DEVPATH}" in
  "/devices/soc/8af8800.usb3/8a00000.dwc3/xhci-hcd.0.auto/usb2/2-1/2-1:1.0/net/eth0")
    IDPROD=$(cat /sys/devices/soc/8af8800.usb3/8a00000.dwc3/xhci-hcd.0.auto/usb2/2-1/idProduct)
    IDVEND=$(cat /sys/devices/soc/8af8800.usb3/8a00000.dwc3/xhci-hcd.0.auto/usb2/2-1/idVendor)
    if [ "$IVEND:$IDPROD" != "0bda:8153" ] ; then
      exit 0
    fi

    abort_if_interface_exists "allsparkg"
    abort_if_interface_exists "${WAN0}"
    abort_if_interface_exists "${LAN0}"
    configure_allspark_g
    ;;
  *"2-1.2"*)
    abort_if_interface_exists "${WAN0}"
    ip link set dev "${INTERFACE}" name "${WAN0}"
    ;;
  *"2-1.3"*)
    abort_if_interface_exists "${LAN0}"
    ip link set dev "${INTERFACE}" name "${LAN0}"
    ;;
  *)
    logger -t "${log_name}" "Ignoring ${INTERFACE}, unknown topology."
    ;;
esac
