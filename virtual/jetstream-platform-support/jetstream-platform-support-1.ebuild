# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2
#
# This package installs stub versions of the libraries defined in
# the platform-support directory in the Jetstream source.  Individual hardware
# platforms should override it and provide useful implementations.

EAPI=4

inherit toolchain-funcs

DESCRIPTION="Jetstream Platform Support package"
HOMEPAGE="http://src.chromium.org"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

DEPEND="chromeos-base/jetstream-platform-api"

src_unpack() {
	cp -a "${FILESDIR}/src" "${S}" || die
}

src_compile() {
	tc-getCC
	emake -C "${S}"/liblightcontrol || die
}

src_install() {
	emake -C "${S}"/liblightcontrol DESTDIR="${D}" install || die
}
