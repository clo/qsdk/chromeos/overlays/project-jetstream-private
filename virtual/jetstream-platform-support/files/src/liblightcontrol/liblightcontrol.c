// Copyright 2015 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// This is a stub implementation that will log operations to syslog, if the
// library is loaded into a binary that has an open syslog connection.

#include "ap/light_control.h"

#include <stdbool.h>
#include <stdlib.h>
#include <syslog.h>

#define LOG_FUNCTION() \
    syslog(LOG_INFO, "%s invoked", __func__);

#define LOG_FUNCTION_PRINTF(format, args...) \
    syslog(LOG_INFO, "%s invoked: " format, __func__, args);

static bool g_developer_mode = false;

void light_init(bool developer_mode) {
  LOG_FUNCTION_PRINTF("developer_mode: %d", developer_mode);
  g_developer_mode = developer_mode;
}

void light_set_developer_mode(bool active) {
  LOG_FUNCTION_PRINTF("set developer_mode to %d", active);
  g_developer_mode = active;
}

bool light_get_developer_mode() {
  LOG_FUNCTION_PRINTF("returning developer_mode: %d", g_developer_mode);
  return g_developer_mode;
}

void light_set_state(state_t state) {
  LOG_FUNCTION_PRINTF("state: %d", state);
}

void light_set_state_with_transition(state_t state, transition_t transition) {
  LOG_FUNCTION_PRINTF("state: %d, transition %d", state, transition);
}

void light_set_master_brightness(uint8_t brightness) {
  LOG_FUNCTION();
}

bool light_get_ambient_brightness(uint8_t *level) {
  LOG_FUNCTION_PRINTF("no sensor available (returning %d)", false);
  return false;
}

const char *light_get_proximity_device_path() {
  return NULL;
}
