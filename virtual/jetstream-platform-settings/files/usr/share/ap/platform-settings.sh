# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# If you are seeing this message, the platform you are building for needs to
# provide an override for the jetstream-platform-settings package.
logger -p user.crit "Required file platform-settings.sh missing"
exit 1
