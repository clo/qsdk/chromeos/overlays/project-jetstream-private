# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

# Note: This package only includes the software packages needed for the
# firewall, not the firewall configuration itself.

EAPI=4

DESCRIPTION="Chrome OS Firewall virtual package"
HOMEPAGE="http://src.chromium.org"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

RDEPEND="
	net-firewall/iptables[ipv6]
"

S="${WORKDIR}"

src_install() {
	insinto /etc/init
	doins "${FILESDIR}"/ip{,6}tables.conf
}
