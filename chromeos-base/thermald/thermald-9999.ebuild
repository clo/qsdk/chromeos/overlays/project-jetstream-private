# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_INCREMENTAL_BUILD=1

CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/thermald"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/thermald"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/thermald"
)
CROS_WORKON_SUBTREE=(
	"common-mk"
	""
)

PLATFORM_SUBDIR="thermald"

inherit cros-workon platform udev user

DESCRIPTION="Thermal daemon"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="~*"
IUSE="test"

RDEPEND="
	chromeos-base/libbrillo
	chromeos-base/metrics
	chromeos-base/minijail
	"

DEPEND="
	${RDEPEND}
	test? (
		dev-cpp/gmock
		dev-cpp/gtest
	)
	"

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/thermald"
}

src_install() {
	dosbin "${OUT}/thermald"

	insinto /etc/init
	doins "init/thermald.conf"

	udev_dorules "udev/99-thermald.rules"
}

platform_pkg_test() {
	platform_test "run" "${OUT}/thermald_unittests"
}

pkg_preinst() {
	enewuser thermald
	enewgroup thermald
}
