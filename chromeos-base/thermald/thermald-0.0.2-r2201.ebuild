# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_COMMIT=("2a2dc1fdf453798d069ab7d3cc543b3fea288ad9" "3b8c66cce9c26903c0de002be3380edcc64b0d55")
CROS_WORKON_TREE=("1d995a5f11b89f06713e6b213ea3f8741ace4008" "16148a4e1afcfa787eec5ff9237191ebd7845c85")
CROS_WORKON_INCREMENTAL_BUILD=1

CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/thermald"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/thermald"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/thermald"
)
CROS_WORKON_SUBTREE=(
	"common-mk"
	""
)

PLATFORM_SUBDIR="thermald"

inherit cros-workon platform udev user

DESCRIPTION="Thermal daemon"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="test"

RDEPEND="
	chromeos-base/libbrillo
	chromeos-base/metrics
	chromeos-base/minijail
	"

DEPEND="
	${RDEPEND}
	test? (
		dev-cpp/gmock
		dev-cpp/gtest
	)
	"

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/thermald"
}

src_install() {
	dosbin "${OUT}/thermald"

	insinto /etc/init
	doins "init/thermald.conf"

	udev_dorules "udev/99-thermald.rules"
}

platform_pkg_test() {
	platform_test "run" "${OUT}/thermald_unittests"
}

pkg_preinst() {
	enewuser thermald
	enewgroup thermald
}
