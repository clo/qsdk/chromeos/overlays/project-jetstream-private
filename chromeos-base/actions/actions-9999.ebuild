# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/actions"
	"platform/ap"
	"platform/ap/wireless"
	"platform/ap-daemons"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/platform/actions"
	"chromeos/ap"
	"chromeos/ap/wireless"
	"chromeos/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/actions"
	"${S}/platform/ap"
	"${S}/platform/ap/wireless"
	"${S}/platform/ap-daemons"
)
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=(
	"common-mk buffet"
	""
	""
	""
	""
)

PLATFORM_SUBDIR="actions"
DESCRIPTION="Jetstream Actions Daemons"

inherit cros-workon generate-upstart-conditions platform user

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="~*"
IUSE="test"

RDEPEND="
	chromeos-base/buffet
	chromeos-base/libbrillo
	chromeos-base/metrics
	chromeos-base/shill
	chromeos-base/webserver
	dev-libs/protobuf
	"

DEPEND="
	${RDEPEND}
	chromeos-base/ap-infra
	dev-cpp/gtest
	"

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/actions"
}

src_install() {
	insinto /etc/
	doins -r "${S}"/etc/*

	generate_upstart_conditions "${D}/etc/init/"*

	dosbin ${OUT}/ap-cloud-actions
	dosbin ${OUT}/ap-local-actions
	dosbin ${OUT}/actions-api-server
}

src_test() {
	platform_test "run" "${OUT}/actions_unittest"
}

pkg_preinst() {
	enewuser ap-cloud-actions
	enewuser ap-local-actions
	enewuser actions-api-server
	enewgroup ap-cloud-actions
	enewgroup ap-local-actions
	enewgroup actions-api-server
}
