# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT=("5b5d5489811aa33f1a74e477096b90eb1f451094" "23b70eec3c75bbd92d94a8a87e6e23ae35f93a8f" "f2f38acc0a3c5af555723ce83e4be60aa591cf2a" "bc527edbc5ac23b6e1a83a397351609e3822f508" "a44db9aedb8f2907536cba85d12e2ae868abf9c9")
CROS_WORKON_TREE=("1d995a5f11b89f06713e6b213ea3f8741ace4008" "c47ce17035d4c2a68c232d10aaa4bd0e6456e2e9" "911f505f9cd815d3700f891efd5dc7a3e3c7a80a" "5eddcf334a3a22449eb16d414576088cef370395" "6b8bb221cace1106c733323355529b6e50eb835b" "0c10ba8f0b8f92c0d79a4942264e57793c0fd388")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/actions"
	"platform/ap"
	"platform/ap/wireless"
	"platform/ap-daemons"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/platform/actions"
	"chromeos/ap"
	"chromeos/ap/wireless"
	"chromeos/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/actions"
	"${S}/platform/ap"
	"${S}/platform/ap/wireless"
	"${S}/platform/ap-daemons"
)
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=(
	"common-mk buffet"
	""
	""
	""
	""
)

PLATFORM_SUBDIR="actions"
DESCRIPTION="Jetstream Actions Daemons"

inherit cros-workon generate-upstart-conditions platform user

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="test"

RDEPEND="
	chromeos-base/buffet
	chromeos-base/libbrillo
	chromeos-base/metrics
	chromeos-base/shill
	chromeos-base/webserver
	dev-libs/protobuf
	"

DEPEND="
	${RDEPEND}
	chromeos-base/ap-infra
	dev-cpp/gtest
	"

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/actions"
}

src_install() {
	insinto /etc/
	doins -r "${S}"/etc/*

	generate_upstart_conditions "${D}/etc/init/"*

	dosbin ${OUT}/ap-cloud-actions
	dosbin ${OUT}/ap-local-actions
	dosbin ${OUT}/actions-api-server
}

src_test() {
	platform_test "run" "${OUT}/actions_unittest"
}

pkg_preinst() {
	enewuser ap-cloud-actions
	enewuser ap-local-actions
	enewuser actions-api-server
	enewgroup ap-cloud-actions
	enewgroup ap-local-actions
	enewgroup actions-api-server
}
