# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_COMMIT="8caded8df7dfb23ad4e838f340119dfbbf3e157f"
CROS_WORKON_TREE="3dbc8507657341b4cd8c4fb0a459268a3be72d9b"
CROS_WORKON_PROJECT="chromeos/vendor/silabs"
CROS_WORKON_LOCALNAME="../partner_private/silabs/"

inherit cros-workon

DESCRIPTION="Silabs library and ip-driver-app used by RF diags. The library has a command interpreter and app manages messaging between spi-server and host app via sockets."

LICENSE="Proprietary"
SLOT="0"
KEYWORDS="*"

src_unpack() {
	cros-workon_src_unpack
	S+="/libwpandiags/files/build"
}

src_install() {
	dolib.so libwpandiags.so
	dobin ip-driver-app
}
