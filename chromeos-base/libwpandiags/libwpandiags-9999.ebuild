# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_PROJECT="chromeos/vendor/silabs"
CROS_WORKON_LOCALNAME="../partner_private/silabs/"

inherit cros-workon

DESCRIPTION="Silabs library and ip-driver-app used by RF diags. The library has a command interpreter and app manages messaging between spi-server and host app via sockets."

LICENSE="Proprietary"
SLOT="0"
KEYWORDS="~*"

src_unpack() {
	cros-workon_src_unpack
	S+="/libwpandiags/files/build"
}

src_install() {
	dolib.so libwpandiags.so
	dobin ip-driver-app
}
