# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

EAPI=5

CROS_WORKON_PROJECT="chromeos/ap-daemons"
CROS_WORKON_LOCALNAME="platform/ap-daemons"
CROS_WORKON_SUBTREE="autotest"

AUTOTEST_PATH="autotest"
AUTOTEST_CLIENT_SITE_TESTS="autotest/client/site_tests"
AUTOTEST_SERVER_SITE_TESTS="autotest/server/site_tests"

inherit cros-workon autotest

DESCRIPTION="Jetstream autotest tests"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~*"
IUSE="+autotest"

RDEPEND="
  chromeos-base/autotest-jetstream-libs
"
DEPEND="${RDEPEND}"

CLIENT_TESTS="
	+tests_jetstream_ApiServerAttestation
	+tests_jetstream_ApiServerDeveloperConfiguration
	+tests_jetstream_ApPortForwarding
	+tests_jetstream_BlockedStations
	+tests_jetstream_BluetoothBeaconing
	+tests_jetstream_DiagnosticReport
	+tests_jetstream_DnsQuery
	+tests_jetstream_FutureBlockedStations
	+tests_jetstream_GcdCommands
	+tests_jetstream_GuestFirewall
	+tests_jetstream_GuestInterfaces
	+tests_jetstream_LocalApi
	+tests_jetstream_LocalHostnames
	+tests_jetstream_NetworkInterfaces
	+tests_jetstream_PortForwarding
	+tests_jetstream_PrefixDelegation
	+tests_jetstream_PrioritizedDevice
	+tests_jetstream_WanCustomDns
"

SERVER_TESTS="
	+tests_jetstream_AutoUpdate
	+tests_jetstream_BlockSingleStation
	+tests_jetstream_BlockStationWithPppoe
	+tests_jetstream_BlockTwoStations
	+tests_jetstream_BlockWiredClient
	+tests_jetstream_BluetoothSanity
	+tests_jetstream_BridgeMode
	+tests_jetstream_ChannelChangeStress
	+tests_jetstream_ClientCount
	+tests_jetstream_ClientFetchUrl
	+tests_jetstream_ClientHistoricalUsage
	+tests_jetstream_CloudBridgeMode
	+tests_jetstream_CloudGroobe
	+tests_jetstream_Connect
	+tests_jetstream_DhcpNaming
	+tests_jetstream_DnsQueriesToUpstreamServer
	+tests_jetstream_EthernetStressPlug
	+tests_jetstream_Groobe
	+tests_jetstream_GroobeInteroperability
	+tests_jetstream_GroupVoobe
	+tests_jetstream_GuestConnectPrimaryConnect
	+tests_jetstream_GuestNetworkAfterApReboot
	+tests_jetstream_GuestNetworkDeviceBoost
	+tests_jetstream_GuestNetworking
	+tests_jetstream_GuestNetworkIsolation
	+tests_jetstream_GuestNetworkSharedStations
	+tests_jetstream_GuestNetworkWithWanConnections
	+tests_jetstream_Insights
	+tests_jetstream_LedBrightness
	+tests_jetstream_MdnsNaming
	+tests_jetstream_Mesh
	+tests_jetstream_MeshExample
	+tests_jetstream_NatLoopback
	+tests_jetstream_Oobe
	+tests_jetstream_OobeViaClient
	+tests_jetstream_Performance
	+tests_jetstream_Pppoe
	+tests_jetstream_PrioritizeDeviceViaCloud
	+tests_jetstream_RealtimeTraffic
	+tests_jetstream_Reboot
	+tests_jetstream_RebootBridgeMode
	+tests_jetstream_RebootViaCloud
	+tests_jetstream_RenameDevice
	+tests_jetstream_SetSsidPsk
	+tests_jetstream_SoftRebootStress
	+tests_jetstream_Voobe
	+tests_jetstream_WanClient
	+tests_jetstream_WanSpeedtestViaCloud
	+tests_jetstream_WanStaticIP
	+tests_jetstream_WanStaticIPViaCloud
	+tests_jetstream_Waveguide
	+tests_jetstream_WeaveConfigChange
	+tests_jetstream_WiredClient
"

IUSE_TESTS="${CLIENT_TESTS} ${SERVER_TESTS}"

IUSE="${IUSE} ${IUSE_TESTS}"

AUTOTEST_FILE_MASK="*.a *.tar.bz2 *.tbz2 *.tgz *.tar.gz"
