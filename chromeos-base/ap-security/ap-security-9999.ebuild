# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/ap"
	"platform/ap/security"
	"platform/ap/wireless"
	"platform/ap-daemons"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/ap"
	"chromeos/ap/security"
	"chromeos/ap/wireless"
	"chromeos/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/ap"
	"${S}/platform/ap/security"
	"${S}/platform/ap/wireless"
	"${S}/platform/ap-daemons"
)
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=(
	"common-mk buffet"
	""
	""
	""
	""
)

PLATFORM_SUBDIR="security"

DESCRIPTION="Jetstream security daemons and scripts."

inherit cros-workon platform user

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="~*"
IUSE="test"

RDEPEND="
	chromeos-base/libbrillo
	chromeos-base/libchrome
	chromeos-base/metrics
	chromeos-base/shill
	chromeos-base/webserver
	dev-libs/re2
	sys-apps/dbus
	"

DEPEND="${RDEPEND}"

src_compile() {
	platform_src_compile
}

src_unpack() {
	local root="${S}"
	platform_src_unpack
	S="${root}/platform/ap/security"
}

src_install() {
	insinto /etc/
	doins -r "${S}"/etc/*
}

src_test() {
	platform_test "run" "${OUT}/ap-security_unittest"
}
