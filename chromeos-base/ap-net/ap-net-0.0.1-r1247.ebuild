# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT=("5cf6ffb0d9c945b55b5816ccdda97194f8a7d4a7" "f2f38acc0a3c5af555723ce83e4be60aa591cf2a")
CROS_WORKON_TREE=("1d995a5f11b89f06713e6b213ea3f8741ace4008" "5eddcf334a3a22449eb16d414576088cef370395")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/ap"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/ap"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/ap"
)
CROS_WORKON_SUBTREE=(
	"common-mk"
	""
)

PLATFORM_SUBDIR="net"
DESCRIPTION="Jetstream Networking related daemons"

inherit cros-workon platform

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="test"

RDEPEND="
	chromeos-base/libbrillo
	chromeos-base/libchrome
	dev-cpp/gflags
	sys-apps/dbus
  "

DEPEND="
	${RDEPEND}
	dev-cpp/gtest
	"

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/ap/net"
}

src_install() {
	insinto /etc/
	doins -r "${S}"/etc/*

	generate_upstart_conditions "${D}/etc/init/"*
	dosbin "${OUT}"/ap-net-monitor
}

src_test() {
	platform_test "run" "${OUT}/ap-net_unittest"
}

pkt_preinst() {
	enewuser ap-net-monitor
	enewgroup ap-net-monitor
}
