#!/bin/sh
# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# This script collects and transmits system information to the Feedback system.

set -e

# The Google Feedback project ID.
PRODUCT_ID=98907

# The work directory for truncating and compressing logs.
WORKROOT=$(mktemp -t -d send-feedback.XXXXXX)

cleanup() {
  rm -rf "${WORKROOT}"
}

trap cleanup EXIT

# The feedback collection system is currently limited to 7MB of payload, so the
# logs are truncated and compressed.
#
# TODO(kemp): This can fail if the truncated log somehow exeeds 7MB.
tail -n 100000 /var/log/messages | gzip > "${WORKROOT}/messages.gz"

GUID=$(uuidgen)
DESCRIPTION="Device logs for report ${GUID}"

feedback_client \
  --desc="${DESCRIPTION}" \
  --product_id="${PRODUCT_ID}" \
  --raw_files="${WORKROOT}/messages.gz"

# Report the GUID to the caller
echo "GUID=${GUID}"
