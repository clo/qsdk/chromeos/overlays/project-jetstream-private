# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

inherit udev osreleased crashid

DESCRIPTION="ChromeOS WiFi Access Point"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

# TODO(kemp): Remove the vpd dependency (crbug.com/p/44865).
RDEPEND="
	app-arch/gzip
	app-shells/dash
	chromeos-base/ap-daemons
	chromeos-base/ap-infra
	chromeos-base/ap-security
	chromeos-base/ap-wireless
	chromeos-base/cryptohome
	chromeos-base/feedback
	chromeos-base/thermald
	chromeos-base/vpd
	dev-util/trace-cmd
	net-dns/dnsmasq
	net-firewall/ebtables
	net-firewall/ipset
	net-misc/bridge-utils
	net-misc/igmpproxy
	net-misc/minissdpd
	net-misc/miniupnpd
	net-wireless/crda
	net-wireless/hostapd
	net-wireless/wireless-regdb
	net-wireless/wpa_supplicant
	sys-apps/ethtool
	!<chromeos-base/ap-daemons-0.0.3
"

S=${WORKDIR}

src_install() {
	dosbin "${FILESDIR}"/usr/sbin/*

	dometricsproductid 3

	docrashid Jetstream
}
