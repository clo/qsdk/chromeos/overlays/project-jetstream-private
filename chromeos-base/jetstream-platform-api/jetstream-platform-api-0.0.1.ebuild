# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2
#
# This package installs the headers that define the platform support API
# required by Jetstream.

EAPI=4

DESCRIPTION="Jetstream Platform API"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

S=${WORKDIR}

src_install() {
	insinto /usr/include
	doins -r "${FILESDIR}"/usr/include/ap
}
