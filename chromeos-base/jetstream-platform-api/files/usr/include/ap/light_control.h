// Copyright 2015 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// This defines the Jetstream light control API.  The implementation of this
// interface is provided by each board overlay (if applicable) and is
// dynamically loaded.  The library name must be "liblightcontrol.so.1".

#ifndef AP_LIGHT_CONTROL_H_
#define AP_LIGHT_CONTROL_H_

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// These patterns are steady state.  Once active, they remain active until a
// state change.
typedef enum {
  STATE_BOOT,
  STATE_NORMAL_OPERATION,
  STATE_WAN_FAILURE,
  STATE_SETUP_REQUIRED,
  STATE_SETUP_ACTIVITY,
  STATE_CRITICAL_FAILURE,
  STATE_VOICE_PROCESSING,
  STATE_VOICE_IO,
  STATE_POWER_WASH,
} state_t;

// These transitions are executed once and then return to a designated steady
// state.
typedef enum {
  TRANSITION_OPERATION_SUCCESSFUL,
  TRANSITION_OPERATION_FAILED,
} transition_t;

// Initialize the light library.  The developer_mode parameter indicates whether
// the device is booted into developer mode or not.
void light_init(bool developer_mode);

// Sets whether the system was booted into developer mode or not.  Since this
// cannot change at runtime, this function is used only for testing.
void light_set_developer_mode(bool active);

// Returns the current value of the developer mode setting, as set by either the
// call to light_init or the most recent call to light_set_developer_mode.
bool light_get_developer_mode();

// Displays a lighting pattern for the provided state.
void light_set_state(state_t state);

// Displays the provieded transition lighting pattern, then displays the pattern
// for the provided state.
void light_set_state_with_transition(state_t state, transition_t transition);

// Sets the brightness of the device lighting where 0 is off and 255 is
// maximum brightness.
void light_set_master_brightness(uint8_t brightness);

// Returns true if the ambient brightness sensor is available, and the
// current ambient brightness level will be returned via the output argument.
bool light_get_ambient_brightness(uint8_t *level);

// Returns the path to the proximity sensor if available.  Returns NULL
// otherwise.
const char *light_get_proximity_device_path();

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // AP_LIGHT_CONTROL_H_
