# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT=("5b5d5489811aa33f1a74e477096b90eb1f451094" "f2f38acc0a3c5af555723ce83e4be60aa591cf2a" "bc527edbc5ac23b6e1a83a397351609e3822f508" "a44db9aedb8f2907536cba85d12e2ae868abf9c9")
CROS_WORKON_TREE=("1d995a5f11b89f06713e6b213ea3f8741ace4008" "c47ce17035d4c2a68c232d10aaa4bd0e6456e2e9" "5eddcf334a3a22449eb16d414576088cef370395" "6b8bb221cace1106c733323355529b6e50eb835b" "0c10ba8f0b8f92c0d79a4942264e57793c0fd388")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/ap"
	"platform/ap/wireless"
	"platform/ap-daemons"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/ap"
	"chromeos/ap/wireless"
	"chromeos/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/ap"
	"${S}/platform/ap/wireless"
	"${S}/platform/ap-daemons"
)
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=(
	"common-mk buffet"
	""
	""
	""
)

PLATFORM_SUBDIR="ap-daemons"

DESCRIPTION="WiFi Access point daemons"

CROS_GO_BINARIES=(
	"chromeos/ap/ap-dns"
	"chromeos/ap/ap-ecm-manager"
	"chromeos/ap/ap-wifi-manager"
	"chromeos/ap/tools/ap-show"
	"chromeos/ap/pfd"
)

CROS_GO_TEST=(
	"chromeos/ap/..."
)

inherit cros-workon generate-upstart-conditions platform cros-go user udev

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

IUSE="test"

RDEPEND="
	chromeos-base/attestation
	chromeos-base/buffet
	chromeos-base/cryptohome
	chromeos-base/libbrillo
	chromeos-base/libchrome
	chromeos-base/minijail
	chromeos-base/shill
	chromeos-base/power_manager
	chromeos-base/power_manager-client
	chromeos-base/update_engine
	chromeos-base/webserver
	dev-cpp/gflags
	dev-cpp/glog
	dev-libs/glib
	dev-libs/libnl
	dev-libs/openssl
	dev-libs/protobuf
	dev-libs/re2
	media-sound/adhd
	net-misc/curl
	net-misc/socat
	net-misc/wget
	net-wireless/iw
	sys-apps/dbus
	sys-apps/iproute2
	virtual/jetstream-platform-settings
	virtual/jetstream-platform-support
	!<chromeos-base/chromeos-ap-0.0.2
	"

DEPEND="
	${RDEPEND}
	chromeos-base/ap-common-go
	chromeos-base/ap-infra
	dev-cpp/gtest
	dev-go/dbus
	dev-go/glog
	dev-go/grpc
	dev-go/net
	dev-go/protobuf
	"

src_compile() {
	platform_src_compile

	# go_proto_files must contain all proto definitions needed by Go binaries,
	# relative to the 'src' directory.
	# Due to http://b/76122182 ap/common/linebacker MUST come before
	# ap/common in proto_paths
	go_proto_files="../../ap/common/linebacker/content_labeling.proto \
	../../ap/common/linebacker/entitlements.proto \
	../../ap/common/ap_dns.proto \
	../../ap/common/storage.proto \
	../../ap/common/runtime_state.proto \
	../../ap/common/storage/datapol/annotations/proto/semantic_annotations.proto \
	../../ap/common/storage/datapol/annotations/proto/st_annotations_example.proto \
	../../ap/common/storage/datapol/annotations/proto/retention_annotations.proto \
	../../ap-daemons/ipv6/ipv6.proto"
	go_proto_package=chromeos/ap/proto

	# replace newlines with ':' to concatenate paths.
	proto_paths=$(dirname ${go_proto_files} | sed -e ':a;N;$!ba;s/\n/:/g' )

	pushd "${S}/src"

	mkdir "./${go_proto_package}"
	protoc --go_out=import_path="${go_proto_package}:./${go_proto_package}" --proto_path="${proto_paths}" --proto_path="../../ap/common" ${go_proto_files}
	protoc --go_out="./" \
			"chromeos/ap/ap-group-monitor/internal/broadcast/packet/packet.proto"
	popd

	cros-go_src_compile
}

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/ap-daemons"
}

src_install() {
	dosbin "${OUT}"/ap-av
	dosbin "${OUT}"/ap-certificate
	dosbin "${OUT}"/ap-configure
	dosbin "${OUT}"/ap-controller
	dosbin "${OUT}"/ap-get-setup-setting
	dosbin "${OUT}"/ap-ipv6-daemon
	dosbin "${OUT}"/ap-lb-ip-filter
	dosbin "${OUT}"/ap-lb-update-manager
	dosbin "${OUT}"/ap-light-control
	dosbin "${OUT}"/ap-log-configure
	dosbin "${OUT}"/ap-monitor
	dosbin "${OUT}"/ap-net-controller
	dosbin "${OUT}"/ap-reflector
	dosbin "${OUT}"/ap-rodizio
	dosbin "${OUT}"/ap-ui-server
	dosbin "${OUT}"/ap-update-manager
	dosbin "${OUT}"/ap-vorlon-client
	dosbin "${OUT}"/rodizio_client
	dosbin "${OUT}"/shill-configure
	dosbin "${OUT}"/speed-test
	dosbin "${OUT}"/whisper
	dosbin "${OUT}"/ap-health-monitor
	dosbin "${S}"/rootfs_overlay/cros/etc/jetstream-cleanup-logs
	dosbin "${S}"/rootfs_overlay/cros/etc/jetstream-update-stats

	insinto /etc/
	doins -r "${S}"/rootfs_overlay/cros/etc/*

	generate_upstart_conditions "${D}/etc/init/"*

	insinto /usr
	doins -r "${S}"/usr/*

	mkdir -p ${D}/etc/ap

	cros-go_src_install
}
src_test() {
	cros-go_src_test

	platform_test "run" "${OUT}/ap-daemons_unittest"
}

pkg_preinst() {
	enewuser ap-certificate
	enewuser ap-controller
	enewuser ap-dns
	enewuser ap-ipv6
	enewuser ap-lb-ip-filter
	enewuser ap-lb-update-manager
	enewuser ap-monitor
	enewuser ap-net-controller
	enewuser ap-pfd
	enewuser ap-reflector
	enewuser ap-rodizio
	enewuser ap-health-monitor
	enewuser ap-update-manager
	enewuser ap-wifi-manager
	enewuser ap-vorlon-client
	enewuser dnsmasq
	enewuser hostapd
	enewuser minissdpd
	enewuser miniupnpd
	enewuser wpa_supplicant
	enewgroup ap-certificate
	enewgroup ap-controller
	enewgroup ap-dns
	enewgroup ap-ipv6
	enewgroup ap-lb-ip-filter
	enewgroup ap-lb-update-manager
	enewgroup ap-monitor
	enewgroup ap-net-controller
	enewgroup ap-pfd
	enewgroup ap-reflector
	enewgroup ap-rodizio
	enewgroup ap-health-monitor
	enewgroup ap-update-manager
	enewgroup ap-wifi-manager
	enewgroup ap-vorlon-client
	enewgroup dnsmasq
	enewgroup hostapd
	enewgroup leds
	enewgroup minissdpd
	enewgroup miniupnpd
	enewgroup wpa_supplicant
}
