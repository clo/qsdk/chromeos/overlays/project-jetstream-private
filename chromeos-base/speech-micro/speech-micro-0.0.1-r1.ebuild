# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT="74adb49d7f22cf8fb20a4c7d62ca0aa100486d82"
CROS_WORKON_TREE="ea8cabe5c01991cf7d12ccb5c7e2c5054b6ef328"
CROS_WORKON_LOCALNAME="third_party/speech-micro"
CROS_WORKON_PROJECT="chromeos/third_party/speech-micro"
DESCRIPTION="Export of //speech/micro"

inherit cros-workon autotools

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

src_prepare() {
	eautoreconf
}
