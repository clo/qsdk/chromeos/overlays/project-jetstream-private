# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_LOCALNAME="third_party/speech-micro"
CROS_WORKON_PROJECT="chromeos/third_party/speech-micro"
DESCRIPTION="Export of //speech/micro"

inherit cros-workon autotools

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="~*"

src_prepare() {
	eautoreconf
}
