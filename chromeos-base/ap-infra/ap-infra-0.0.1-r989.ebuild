# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT=("5b5d5489811aa33f1a74e477096b90eb1f451094" "f2f38acc0a3c5af555723ce83e4be60aa591cf2a" "bc527edbc5ac23b6e1a83a397351609e3822f508" "a44db9aedb8f2907536cba85d12e2ae868abf9c9")
CROS_WORKON_TREE=("1d995a5f11b89f06713e6b213ea3f8741ace4008" "c47ce17035d4c2a68c232d10aaa4bd0e6456e2e9" "5eddcf334a3a22449eb16d414576088cef370395" "6b8bb221cace1106c733323355529b6e50eb835b" "0c10ba8f0b8f92c0d79a4942264e57793c0fd388")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/ap"
	"platform/ap/wireless"
	"platform/ap-daemons"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/ap"
	"chromeos/ap/wireless"
	"chromeos/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/ap"
	"${S}/platform/ap/wireless"
	"${S}/platform/ap-daemons"
)
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=(
	"common-mk buffet"
	""
	""
	""
)

PLATFORM_SUBDIR="ap-infra"
DESCRIPTION="Jetstream infrastructure daemons and libs shared across products"

inherit cros-workon platform user generate-upstart-conditions

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="test"

RDEPEND="
	chromeos-base/attestation
	chromeos-base/buffet
	chromeos-base/libbrillo
	chromeos-base/libchrome
	chromeos-base/metrics
	chromeos-base/shill
	chromeos-base/webserver
	dev-cpp/gflags
	dev-libs/protobuf
	!chromeos-base/ap-flags
	!chromeos-base/ap-monlog
	"

DEPEND="
	${RDEPEND}
	dev-cpp/gtest
	"

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/ap"
}

src_install() {
	dosbin "${OUT}"/ap-api-server
	dosbin "${OUT}"/ap-attestation-client
	dosbin "${OUT}"/ap-diagnostics
	dosbin "${OUT}"/ap-flags
	dosbin "${OUT}"/ap-hal
	dosbin "${OUT}"/ap-monlog
	dosbin "${OUT}"/ap-pipe-reader
	dosbin "${OUT}"/hal-client

	insinto /etc/
	doins -r "${S}"/rootfs_overlay/cros/etc/*

	insinto /etc/ap/
	doins -r "${OUT}"/job_triggers

	generate_upstart_conditions "${D}/etc/init/"*
}

src_test() {
	platform_test "run" "${OUT}/ap-infra_unittests"
}

pkg_preinst() {
	enewuser ap-api-server
	enewuser ap-diagnostics
	enewuser ap-flags
	enewuser ap-hal
	enewuser ap-monlog
	enewuser ap-pipe-reader
	enewgroup ap-api-server
	enewgroup ap-diagnostics
	enewgroup ap-flags
	enewgroup ap-hal
	enewgroup ap-monlog
	enewgroup ap-pipe-reader
}
