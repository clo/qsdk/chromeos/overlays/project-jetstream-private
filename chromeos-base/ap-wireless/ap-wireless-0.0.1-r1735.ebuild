# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

CROS_WORKON_COMMIT=("5b5d5489811aa33f1a74e477096b90eb1f451094" "f2f38acc0a3c5af555723ce83e4be60aa591cf2a" "bc527edbc5ac23b6e1a83a397351609e3822f508" "a44db9aedb8f2907536cba85d12e2ae868abf9c9")
CROS_WORKON_TREE=("1d995a5f11b89f06713e6b213ea3f8741ace4008" "c47ce17035d4c2a68c232d10aaa4bd0e6456e2e9" "5eddcf334a3a22449eb16d414576088cef370395" "6b8bb221cace1106c733323355529b6e50eb835b" "0c10ba8f0b8f92c0d79a4942264e57793c0fd388")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME=(
	"platform2"
	"platform/ap"
	"platform/ap/wireless"
	"platform/ap-daemons"
)
CROS_WORKON_PROJECT=(
	"chromiumos/platform2"
	"chromeos/ap"
	"chromeos/ap/wireless"
	"chromeos/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform2"
	"${S}/platform/ap"
	"${S}/platform/ap/wireless"
	"${S}/platform/ap-daemons"
)
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE=(
	"common-mk buffet"
	""
	""
	""
)

PLATFORM_SUBDIR="wireless"

DESCRIPTION="Jetstream wireless radio configuration and monitoring daemons"

CROS_GO_BINARIES=(
	"chromeos/ap/ap-wifi-diagnostics"
)

CROS_GO_TEST=(
	"chromeos/ap/..."
)

inherit cros-workon generate-upstart-conditions platform cros-go user

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="test"

RDEPEND="
	chromeos-base/libbrillo
	chromeos-base/shill
	chromeos-base/webserver
	dev-cpp/gflags
	sys-apps/dbus
	"

DEPEND="
	${RDEPEND}
	dev-cpp/gtest
	dev-go/dbus
	dev-go/fsnotify
	"

src_compile() {
	platform_src_compile

	cros-go_src_compile
}

src_unpack() {
	local s="${S}"
	platform_src_unpack
	S="${s}/platform/ap/wireless"
}

src_install() {
	insinto /etc/
	doins -r "${S}"/rootfs_overlay/cros/etc/*

	generate_upstart_conditions "${D}/etc/init/"*

	dosbin "${OUT}"/ap-backhaul-manager
	dosbin "${OUT}"/ap-group-manager
	dosbin "${OUT}"/ap-group-monitor
	dosbin "${OUT}"/ap-wireless-optimizer
	dosbin "${OUT}"/ap-wifiblaster-daemon
	dosbin "${OUT}"/wifiblaster

	cros-go_src_install
}

src_test() {
	cros-go_src_test

	platform_test "run" "${OUT}/ap-wireless_unittest"
}

pkg_preinst() {
	enewuser ap-group-manager
	enewuser ap-backhaul-manager
	enewuser ap-wireless-optimizer
	enewuser ap-wifi-diagnostics
	enewuser ap-group-monitor

	enewgroup ap-group-manager
	enewgroup ap-backhaul-manager
	enewgroup ap-wireless-optimizer
	enewgroup ap-wifi-diagnostics
	enewgroup ap-group-monitor
}
