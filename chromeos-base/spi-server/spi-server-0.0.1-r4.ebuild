# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_COMMIT="8caded8df7dfb23ad4e838f340119dfbbf3e157f"
CROS_WORKON_TREE="3dbc8507657341b4cd8c4fb0a459268a3be72d9b"
CROS_WORKON_PROJECT="chromeos/vendor/silabs"
CROS_WORKON_LOCALNAME="../partner_private/silabs/"

inherit toolchain-funcs cros-workon

DESCRIPTION="SPI server provided by Silabs to communicate with 15.4 radio. Used only by RF testing tools at this point, and not to be shipped to end user."

LICENSE="Proprietary"
SLOT="0"
KEYWORDS="*"

src_unpack() {
	cros-workon_src_unpack
	S+="/spi-server/files"
}

src_compile() {
	tc-export CC CXX
	emake
}

src_install() {
	dobin spi-server
}
