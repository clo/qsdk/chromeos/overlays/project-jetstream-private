# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_PROJECT="chromeos/vendor/silabs"
CROS_WORKON_LOCALNAME="../partner_private/silabs/"

inherit toolchain-funcs cros-workon

DESCRIPTION="SPI server provided by Silabs to communicate with 15.4 radio. Used only by RF testing tools at this point, and not to be shipped to end user."

LICENSE="Proprietary"
SLOT="0"
KEYWORDS="~*"

src_unpack() {
	cros-workon_src_unpack
	S+="/spi-server/files"
}

src_compile() {
	tc-export CC CXX
	emake
}

src_install() {
	dobin spi-server
}
