# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="Go support libraries for Jetstream"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="~*"
IUSE="test"

CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform/ap"
CROS_WORKON_PROJECT="chromeos/ap"

CROS_GO_PACKAGES=(
	"chromeos/ap/syslog"
	"chromeos/ap/uma"
)

inherit cros-workon cros-go libchrome

RDEPEND="
	chromeos-base/metrics
	"

DEPEND="${RDEPEND}"

src_unpack() {
	cros-workon_src_unpack

	S="${S}/common"

	# TODO(kemp): This is terrible, but it saves any package that depends on
	# this package from having to set a CGO_LDFLAGS variable.
	sed -i -e "s/##BASE_VERSION##/${BASE_VER}/" \
		"${S}/src/chromeos/ap/uma/uma.go"
}
