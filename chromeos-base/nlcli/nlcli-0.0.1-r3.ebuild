# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=4

CROS_WORKON_COMMIT="2e105a58dff2779e001042dcc0c1f74c936a0926"
CROS_WORKON_TREE="e8f9bd984a73e1eba723770deacfd8cc86b4ae2c"
CROS_WORKON_PROJECT="chromeos/third_party/wpan-tools"
CROS_WORKON_LOCALNAME="../third_party/wpan-tools/"

inherit toolchain-funcs cros-workon

DESCRIPTION="Nest Labs 15.4 RF diags command line interface"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

DEPEND="chromeos-base/libwpandiags"
RDEPEND=${DEPEND}

src_unpack() {
	cros-workon_src_unpack
	S+="/wpan-diags/files"
}

src_compile() {
	tc-export CC CXX
	emake
}

src_install() {
	dobin nlcli
}
