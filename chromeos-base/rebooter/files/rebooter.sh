#!/bin/bash

# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

interface="br-lan"
suppression_file="/mnt/stateful_partition/rebooter.stop"
identity_interface="eth0"
sampling_interval="10m"
update_delay="6h"
reboot_threshold=$((10 * 1024 * 1024))

read_counter() {
  local interface=$1
  local name=$2
  cat /sys/class/net/${interface}/statistics/${name}
}

cumulative_transfer() {
  local rx=$(read_counter ${interface} 'rx_bytes')
  local tx=$(read_counter ${interface} 'tx_bytes')
  echo $((${tx} + ${rx}))
}

hardware_address() {
  local interface=$1
  cat /sys/class/net/${interface}/address
}

rebooter_enabled() {
  if [ -r "${suppression_file}" ]; then
    return 1
  fi
  local address=$(hardware_address "${identity_interface}")
  local byte=$(echo "${address}" | md5sum | cut -b1,2)
  return $((16#${byte} > 25))
}

if ! rebooter_enabled; then
  logger "Rebooter not enabled on this device."
  exit 0
fi

sleep "${update_delay}"

while [ true ]; do
  initial_transfer=$(cumulative_transfer)
  sleep "${sampling_interval}"
  final_transfer=$(cumulative_transfer)

  delta=$((${final_transfer} - ${initial_transfer}))
  if (( ${delta} < ${reboot_threshold} )); then
    logger "Rebooting: ${delta} bytes sent."
    reboot
  fi
done
