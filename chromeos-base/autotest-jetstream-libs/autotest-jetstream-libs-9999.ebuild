# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

EAPI=4

CROS_WORKON_PROJECT=(
	"chromeos/ap"
	"chromeos/ap-daemons"
)
CROS_WORKON_LOCALNAME=(
	"platform/ap"
	"platform/ap-daemons"
)
CROS_WORKON_DESTDIR=(
	"${S}/platform/ap"
	"${S}/platform/ap-daemons"
)

inherit cros-workon

DESCRIPTION="Jetstream autotest support code"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~*"

RDEPEND=""
DEPEND="${RDEPEND}"

UNITTESTS=(
	"autotest_lib.server.cros.jetstream.external.iperf_parser_unittest"
)

src_compile() {
	protoc --proto_path="${S}" \
		"${S}"/platform/ap/diagnostics/diagnostic_report.proto \
		--python_out="${T}"
}

src_install() {
	insinto "${AUTOTEST_BASE}"/client
	doins -r "${S}"/platform/ap-daemons/autotest/client/cros
	doins -r "${S}"/platform/ap-daemons/autotest/client/common_lib
	insinto "${AUTOTEST_BASE}"/server
	doins -r "${S}"/platform/ap-daemons/autotest/server/cros
	insinto "${AUTOTEST_BASE}"
	doins -r "${S}"/platform/ap-daemons/autotest/test_suites

	insinto "${AUTOTEST_BASE}"/client/common_lib/cros/jetstream

	doins -r "${T}"/platform/ap/diagnostics/*.py
}

src_test() {
	# A preferred approach would be to test using a full autotest
	# tree, running the unit tests via unittest_suite.  However,
	# since autotest has a dependency on this package via the
	# virtual autotest-libs, we can't bring in autotest without
	# creating a dependency cycle. So for now, unit tests must
	# not depend on autotest code.

	testdir="${T}"/test_runner
	mkdir "${testdir}"
	rsync -aL ${S}/platform/ap-daemons/autotest/ "${testdir}"/autotest_lib
	pushd "${testdir}"

	touch autotest_lib/__init__.py
	touch autotest_lib/server/__init__.py
	touch autotest_lib/server/cros/__init__.py
	for unit_test in "${UNITTESTS[@]}"; do
		python -m "${unit_test}" || die "unit test failed"
	done
	popd
}
