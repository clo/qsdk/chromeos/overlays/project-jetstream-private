# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# @ECLASS: generate-upstart-conditions.eclass
# @MAINTAINER:
# The Chromium OS Authors. <chromium-os-dev@chromium.org>
# @BUGREPORTS:
# Please report bugs via http://crosbug.com/p/new?template=Jetstream%20Bugs
# @VCSURL:
# https://chrome-internal.googlesource.com/chromeos/overlays/project-jetstream-private/+/master/eclass/@ECLASS
# @BLURB: Generates Upstart conditions for ap-daemons jobs which use AUTOMATIC.

# TODO(kemp): Once Go support is introduced, rewrite this mess.

write_event_block() {
	local event_name="$1-ap-services"
	if [[ "$1" == "start" ]] && [[ $# -eq 1 ]]; then
		echo "  started system-services or"
	fi
	if [[ "$1" == "stop" ]] || [[ $# -eq 1 ]]; then
		set -- "$@" "FACTORY_RESET=true"
	fi
	if [[ $# -gt 2 ]]; then
		printf "  ${event_name} %s or\n" "${@:2:$#-2}"
	fi
	echo "  ${event_name} ${*: -1}"
}

# @FUNCTION: generate-upstart-conditions
# @USAGE: <files>
# @DESCRIPTION:
# Generates Upstart conditions for ap-daemons jobs which use AUTOMATIC.
#
# This tool replaces "start/stop on AUTOMATIC" conditions in upstart jobs and
# overrides.
#
# Multi-line 'start' and 'stop' input stanzas are not supported currently
# (though the tool generates them).
generate_upstart_conditions() {
	local input_files filename output_filename
	local settings start_condition stop_condition

	input_files=$(grep -l -E '(start|stop).*AUTOMATIC' "$@")
	for filename in ${input_files}; do
		echo "Updating $(basename "${filename}")"
		settings=$(grep -o -e 'SETTING_[0-9A-Z_]*' "${filename}" | sort -u |
			sed -e 's/$/_CHANGED=true/')
		start_condition=$(
			echo "("
			write_event_block "start" ${settings}
			echo ")"
		)
		stop_condition=$(
			echo "("
			write_event_block "stop" ${settings}
			echo ") or stopping system-services"
		)

		# TODO(kemp): I would rather expand these inline below, but it does not work
		# as I would expect.
		start_condition="${start_condition//$'\n'/\\n}"
		stop_condition="${stop_condition//$'\n'/\\n}"

		output_filename="${T}/generate-upstart"
		awk -f - "${filename}" <<EOF >"${output_filename}" || die
			/^start on.*AUTOMATIC.*$/ {
				print gensub(/AUTOMATIC/, "${start_condition}", 1);
				next
			}
			/^stop on.*AUTOMATIC.*$/ {
				print gensub(/AUTOMATIC/, "${stop_condition}", 1);
				next
			}
			{ print }
EOF

		cp "${output_filename}" "${filename}" || die
	done
}
